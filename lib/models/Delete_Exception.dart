class DeleteException implements Exception {
  String message ;
  DeleteException(this.message) ;

  @override
  String toString() {
    return message ;
  }

}