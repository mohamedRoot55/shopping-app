import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:shop_app_mohamed_ahmed_hassan/Routes/ProductInfo.dart';
import '../Providers/Products.dart';
import 'package:provider/provider.dart';
import 'package:pigment/pigment.dart';
import '../Providers/Carts.dart';
import '../Providers/Auth.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final AuthData = Provider.of<Auth>(context, listen: false);
    return Container(
      child: GridTile(
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(ProductInfo.RouteName, arguments: {
              'id':product.id ,
              'title': product.title,
              'price': product.price,
              'description': product.description,
              'imageUrl': product.imageUrl,
            });
          },

              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Hero(
                  tag: product.id,
                  child: FadeInImage(
                    placeholder:
                        AssetImage('assets/images/5.2 product-placeholder.png'),
                    image: NetworkImage(product.imageUrl),
                    fit: BoxFit.cover,
                  ),
                ),

            ),

        ),
        footer: Container(
          color: Pigment.fromString("#FF005C"),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: (product.isFavorite)
                    ? Icon(
                        Icons.favorite,
                        color: Colors.orange,
                      )
                    : Icon(
                        Icons.favorite_border,
                        color: Colors.orange,
                      ),
                onPressed: () {
                  product.toggleFavoriteStatus(AuthData.token, AuthData.UserId);
                },
              ),
              Expanded(
                  child: Text(
                product.title,
                style: TextStyle(color: Colors.white),
              )),
              Consumer<Carts>(
                builder: (_, carts, child) {
                  return IconButton(
                    onPressed: () {
                      carts.addNewCart(product.id, product.title, product.price,
                          DateTime.now());
                      prefix0.Scaffold.of(context).hideCurrentSnackBar();
                      Scaffold.of(context).showSnackBar(SnackBar(
                        content: Text(
                          'you added new item to your carts',
                          style: TextStyle(color: Colors.orange),
                        ),

                      ));
                    },
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.orange,
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
