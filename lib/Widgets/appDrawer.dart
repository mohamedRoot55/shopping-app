import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Routes/OrdersScreen.dart';
import '../Routes/ViewAllProducts.dart';
import '../Routes/yourProducts.dart';
import 'package:provider/provider.dart';
import '../Providers/Auth.dart';
import '../helpers/CustomRoute.dart';
import '../Routes/Auth_Screen.dart';

class appDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text('hello my friend'),
            automaticallyImplyLeading: false,
          ),
          ListTile(
            leading: Icon(Icons.shopping_cart),
            title: Text("Shopping"),
            onTap: () {
              Navigator.of(context)
                  .pushReplacementNamed(ViewAllProducts.RouteName);
            },
          ),
          ListTile(
            leading: Icon(Icons.add_shopping_cart),
            title: Text("Orders"),
            onTap: () {
              Navigator.of(context)
                  .pushReplacementNamed(OrdersScreen.routeName);
//              Navigator.of(context).pushReplacement(CutomRoute(builder: (ctx) {
//                return OrdersScreen();
//               } ,) ,);
            },
          ),
          ListTile(
            leading: Icon(Icons.shopping_basket),
            title: Text("your products"),
            onTap: () {
              Navigator.of(context)
                  .pushReplacementNamed(YourProducts.RouteName);
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text("Logout"),
            onTap: () {
              Navigator.of(context).pop();
              // Navigator.of(context).pushReplacementNamed(AuthScreen.routeName);
              Provider.of<Auth>(context, listen: false).logout();
            },
          )
        ],
      ),
    );
  }
}
