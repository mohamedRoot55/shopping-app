import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import '../models/Delete_Exception.dart';

class Product with ChangeNotifier {
  final String title;

  final double price;

  final String id;

  final String description;

  final String imageUrl;

  bool isFavorite;

  Product(
      {@required this.id,
      @required this.title,
      @required this.description,
      @required this.price,
      @required this.imageUrl,
      this.isFavorite = false});


  void _setFavValue(bool newValue) {
    isFavorite = newValue;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus( String authToken , String userId) async {
    final oldStatus = isFavorite;
    isFavorite = !isFavorite;
    notifyListeners();
    final url = 'https://shopapp-da79d.firebaseio.com/userFavorites/$userId/$id.json?auth=$authToken';
    try {
      final response = await http.put(
        url,
        body: json.encode(isFavorite),

      );
    //  print(json.decode(response.body))  ;
      if (response.statusCode >= 400) {
        _setFavValue(oldStatus);
      }
    } catch (error) {
      _setFavValue(oldStatus);
    }
  }
}



class Products with ChangeNotifier {
  List<Product> _products_data = [];

  String authToken;
  var userId ;

  Products(this.authToken , this.userId);

  List<Product> get allData {
    return [..._products_data];
  }

  List<Product> _Favorites = [];

  List<Product> get OnlyFavorite {
    return _products_data.where((product) {
      return product.isFavorite == true;
    }).toList();
  }

  void addFavorite(Product product) {
    _Favorites.add(product);
  }

  Product findById(String id) {
    Product product = _products_data.firstWhere((product) {
      return product.id == id;
    });
    return product;
  }

  Future<void> UpdateProduct(String id, Product product) async {
    final String url =
        "https://shopapp-da79d.firebaseio.com/products/$id.json?auth=$authToken";
    int ProductIndex = _products_data.indexWhere((product) {
      return product.id == id;
    });
    await http.patch(url,
        body: json.encode({
          'title': product.title,
          'price': product.price,
          'description': product.description,
          'imageUrl': product.imageUrl,
        }));
    _products_data[ProductIndex] = product;
    notifyListeners();
  }

  Future<void> addNewProduct(Product product) async {
    final String url =
        "https://shopapp-da79d.firebaseio.com/products.json?auth=$authToken";
    try {
      await http.post(url,
          body: json.encode({
            'title': product.title,
            'price': product.price,
            'description': product.description,
            'imageUrl': product.imageUrl,
            'CreatorId': userId
          }));
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> FetchDataFromServer([bool isMine = false]) async {
     String isMineOrNot =(isMine) ? 'orderBy="CreatorId"&equalTo="$userId"': '';
    final  url =
        "https://shopapp-da79d.firebaseio.com/products.json?auth=$authToken&$isMineOrNot";
    try {
      final response = await http.get(url);
      final LoadedData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> ExtractedProducts = [];
      final urlFav = 'https://shopapp-da79d.firebaseio.com/userFavorites/$userId/.json?auth=$authToken';
       final FavoriteResponse = await http.get(urlFav) ;
       final FavoriteData = json.decode(FavoriteResponse.body) ;
       //  print(FavoriteResponse) ;
      LoadedData.forEach((productId, productData) {
        ExtractedProducts.add(Product(
            id: productId,
            title: productData['title'],
            description: productData['description'],
            price: productData['price'],
            imageUrl: productData['imageUrl'],
            isFavorite: FavoriteData == null ? false : FavoriteData[productId] ?? false
        ));
      });
      _products_data = ExtractedProducts;
      notifyListeners();
    } catch (error) {
    //  throw error;
    }
  }

  Future<void> deleteSingleProduct(String id) async {
    final String url =
        "https://shopapp-da79d.firebaseio.com/products/$id.json?auth=$authToken";
    final productIndex = _products_data.indexWhere((product) {
      return product.id == id;
    });
    var product = _products_data[productIndex];

    await http.delete(url).then((response) {
      if (response.statusCode >= 400) {
        throw DeleteException('faild to delete product');
      }
      notifyListeners();
      product = null;
    }).catchError((error) {
      _products_data.add(product);
      notifyListeners();
    });
    //  todo : just revision this code
    _products_data.removeAt(productIndex);
    notifyListeners();
  }
}
