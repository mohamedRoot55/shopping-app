import 'package:flutter/cupertino.dart';
import 'package:shop_app_mohamed_ahmed_hassan/Providers/Products.dart';

class Cart {
  final String id;

  final String title;

  final double price;

  final DateTime dateTime;

  int amount;

  Cart(
      {@required this.id,
      @required this.title,
      @required this.dateTime,
      @required this.price,
      this.amount});
}

class Carts with ChangeNotifier {
  Map<String, Cart> _allCarts = {};

  Map<String, Cart> getCarts() {
    return {..._allCarts};
  }

  void addNewCart(String id, String title, double price, DateTime dateTime) {
    if (_allCarts.containsKey(id)) {
      _allCarts.update(id, (existingProduct) {
        return Cart(
            id: existingProduct.id,
            title: existingProduct.title,
            dateTime: existingProduct.dateTime,
            price: existingProduct.price,
            amount: existingProduct.amount + 1);
      });
    } else {
      _allCarts.putIfAbsent(id, () {
        return Cart(
            dateTime: dateTime,
            price: price, title: title, id: id, amount: 1);
      });
    }
    notifyListeners();
  }

  double get allPrice {
    double totalPrice = 0;
    _allCarts.forEach((productId, cart) {
      totalPrice += cart.price;
    });
    return totalPrice;
  }

  void deleteAllCarts (){
    _allCarts = {} ;
    notifyListeners() ;

  }
  void deleteSingleCart(String id ){
    _allCarts.remove(id) ;
    notifyListeners() ;
  }
}
