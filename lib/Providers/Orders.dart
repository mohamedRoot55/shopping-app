import 'package:flutter/material.dart';
import '../Providers/Carts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Order {
  final String id;
  final double totalAmount;

  final DateTime dateTime;

  final List<Cart> carts;

  Order({this.carts, this.dateTime, this.id, this.totalAmount});
}

class Orders with ChangeNotifier {
  String userId;

  List<Order> _allOrders = [];
  final String token;

  Orders(this.userId, this.token, this._allOrders);

  List<Order> get getOrders {
    return [..._allOrders];
  }

  Future<void> addNewOrder(String id, double totalAmount, DateTime dateTime,
      List<Cart> carts) async {
    final String url =
        'https://shopapp-da79d.firebaseio.com/orders/$userId.json?auth=$token';
    try {
      await http.post(url,
          body: json.encode({
            'totalAmount': totalAmount,
            'DateTime': dateTime.toIso8601String(),
            'CreatorId': userId,
            'Carts': carts.map((cart) {
              return {
                'id': cart.id,
                'title': cart.title,
                'price': cart.price,
                'amount': cart.amount,
                'datetime': cart.dateTime.toIso8601String(),

              };
            }).toList(),
          }));
    } catch (error) {
      throw error;
    }

    notifyListeners();
  }

  Future<void> FetchDataFromServer() async {
    final String url =
        'https://shopapp-da79d.firebaseio.com/orders/$userId.json?auth=$token';
    final response = await http.get(url);
    // print(response.body);

    final loadedData = json.decode(response.body) as Map<String, dynamic>;

    if (loadedData == null) {
      return;
    }

    final List<Order> extractedData = [];

    loadedData.forEach((orderId, orderData) {
      extractedData.add(Order(
          id: orderId,
          dateTime: DateTime.parse(orderData['DateTime']),
          totalAmount: orderData['totalAmount'],
          carts: (orderData['Carts'] as List<dynamic>).map((item) {
            return Cart(
                dateTime: DateTime.parse(item['datetime']),
                id: item['id'],
                price: item['price'],
                title: item['title'],
                amount: item['amount']);
          }).toList()));

      _allOrders = extractedData;
      notifyListeners();
    });
  }
}
