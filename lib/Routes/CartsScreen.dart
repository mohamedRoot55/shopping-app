import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Providers/Carts.dart';
import 'package:pigment/pigment.dart';
import 'package:intl/intl.dart';
import '../Providers/Orders.dart';

class CartsScreen extends StatefulWidget {
  static const String RouteName = '/CartsScreen';

  @override
  _CartsScreenState createState() => _CartsScreenState();
}

class _CartsScreenState extends State<CartsScreen> {
  @override
  Widget build(BuildContext context) {
    /////////////////////////////////
    final cartProvider = Provider.of<Carts>(context);
    final carts = Provider.of<Carts>(context).getCarts();
    List<Cart> cartsList = carts.values.toList();

    bool isLoading = false ;

    ///////////////////////////////////////

    return Scaffold(
      //backgroundColor: Pigment.fromString('#FF005C'),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Pigment.fromString("#3F0B81"),
        centerTitle: true,
        title: Text(
          'Carts',
          style: TextStyle(color: Pigment.fromString("#FFFFFF")),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text('TotalPrice'),
              Chip(
                label: Text('${cartProvider.allPrice.toStringAsFixed(2)}'),
              ),
              Consumer<Orders>(
                builder: (_, orders, child) {
                  return  FlatButton(
                    child: isLoading ? CircularProgressIndicator() : Text('Order Now'),
                    onPressed: (cartProvider.allPrice <= 0 || isLoading) ? null :  () async {
                      setState(() {
                        isLoading = true ;
                      });
                     await orders.addNewOrder(DateTime.now().toString(),
                          cartProvider.allPrice, DateTime.now(), cartsList);
                     setState(() {
                       isLoading = false ;
                     });

                      cartProvider.deleteAllCarts();
                    },
                    highlightColor: Colors.teal,
                  );
                },
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
                itemCount: cartsList.length,
                itemBuilder: (_, index) {
                  return Dismissible(
                    key: ValueKey(DateTime.now()),
                    confirmDismiss: (direction) {
                      return showDialog(
                          context: context,
                          builder: (_) {
                            return AlertDialog(
                              title: Text('are you sure'),
                              content: Text('you want to delete this cart!!'),
                              actions: <Widget>[
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(true) ;
                                  },
                                  child: Text('yes'),
                                ),
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(false) ;
                                  },
                                  child: Text('no'),
                                )
                              ],
                            );
                          });
                    },
                    onDismissed: (direction) {
                      cartProvider.deleteSingleCart(cartsList[index].id);
                    },
                    child: ListTile(
                      leading: CircleAvatar(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FittedBox(
                          child: Text('${cartsList[index].price}'),
                        ),
                      )),
                      title: Text("${cartsList[index].title}"),
                      subtitle: Text(
                          "${DateFormat.yMMMd().format(cartsList[index].dateTime)}"),
                      trailing: Container(
                          width: 100,
                          child: Row(
                            children: <Widget>[
                              Text('${cartsList[index].amount} x'),
                              IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {},
                              )
                            ],
                          )),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
