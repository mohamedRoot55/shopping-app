import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Providers/Products.dart';
import 'package:provider/provider.dart';

class EditOrAddNewProduct extends StatefulWidget {
  static const String RouteName = '/EditOrAddNewProduct';

  @override
  _EditOrAddNewProductState createState() => _EditOrAddNewProductState();
}

class _EditOrAddNewProductState extends State<EditOrAddNewProduct> {
  ////////////////////////////////////
  Map<String, Object> values = {
    'title': '',
    'price': '',
    'description': '',
    'imageUrl': ''
  };

  var UrlController = TextEditingController();
  var FormKey = GlobalKey<FormState>();
  FocusNode titleFocus = new FocusNode();

  FocusNode priceFocus = new FocusNode();

  FocusNode descriptionFocus = new FocusNode();

  FocusNode imageFocus = new FocusNode();
  String productId = null;

  bool isLoading = false;

  Product Editedproduct =
  new Product(title: '',
      id: null,
      price: 0,
      description: '',
      imageUrl: '');

  @override
  void dispose() {
    super.dispose();
    titleFocus.dispose();
    priceFocus.dispose();
    descriptionFocus.dispose();
    UrlController.dispose();
    imageFocus.dispose();
  }

  @override
  void initState() {
    imageFocus.addListener(UpdateImageFocus);
    super.initState();
  }

  void UpdateImageFocus() {
    if (!imageFocus.hasFocus) {
      if ((!UrlController.text.startsWith('http') &&
          !UrlController.text.startsWith('https')) ||
          (!UrlController.text.endsWith('.png') &&
              !UrlController.text.endsWith('.jpg') &&
              !UrlController.text.endsWith('.jpeg'))) {
        return;
      }
      setState(() {});
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    productId = ModalRoute
        .of(context)
        .settings
        .arguments as String;
    if (productId != null) {
      var productsProvider = Provider.of<Products>(context);
      Editedproduct = productsProvider.findById(productId);
    }

    values['title'] = Editedproduct.title;
    values['price'] = Editedproduct.price.toString();
    values['description'] = Editedproduct.description;
    // values['imageUrl'] = Editedproduct.imageUrl ;
    UrlController.text = Editedproduct.imageUrl;
  }

  Future<void> _saveForm() async {
    var productsProvider = Provider.of<Products>(context, listen: false);
    bool isValidate = FormKey.currentState.validate();
    if (!isValidate) {
      return;
    }
    FormKey.currentState.save();
    setState(() {
      isLoading = true;
    });

    if (productId != null) {
      await Provider.of<Products>(context)
          .UpdateProduct(productId, Editedproduct);
    } else {
      try {
        await productsProvider.addNewProduct(Editedproduct);
      } catch (error) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('an error has occured'),
        ));
      }
    }
    setState(() {
      isLoading = false;
    });

    Navigator.of(context).pop();
  }

  ///////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('edit or adding Screen'),
        centerTitle: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: _saveForm,
          )
        ],
      ),
      body: (isLoading)
          ? Center(
        child: CircularProgressIndicator(),
      )
          : Container(
          padding: const EdgeInsets.all(15),
          child: Form(
            key: FormKey,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    initialValue: values['title'],
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(priceFocus);
                    },
                    decoration: InputDecoration(
                      labelText: 'titel',
                    ),
                    textInputAction: TextInputAction.next,
                    onSaved: (value) {
                      Editedproduct = Product(
                          id: Editedproduct.id,
                          imageUrl: Editedproduct.imageUrl,
                          description: Editedproduct.description,
                          price: Editedproduct.price,
                          title: value);
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'please enter product title';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    initialValue: values['price'],
                    focusNode: priceFocus,
                    decoration: InputDecoration(
                      labelText: 'price',
                    ),
                    textInputAction: TextInputAction.next,
                    onSaved: (value) {
                      Editedproduct = Product(
                          id: Editedproduct.id,
                          imageUrl: Editedproduct.imageUrl,
                          description: Editedproduct.description,
                          price: double.parse(value),
                          title: Editedproduct.title);
                    },
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(descriptionFocus);
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'please enter price';
                      } else if (double.tryParse(value) == null) {
                        return 'please enter validate price';
                      } else if (double.parse(value) <= 0) {
                        return 'please enter validate price';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    initialValue: values['description'],
                    focusNode: descriptionFocus,
                    decoration: InputDecoration(
                      labelText: 'description',
                    ),
                    maxLines: 3,
                    onSaved: (value) {
                      Editedproduct = Product(
                          id: Editedproduct.id,
                          imageUrl: Editedproduct.imageUrl,
                          description: value,
                          price: Editedproduct.price,
                          title: Editedproduct.title);
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'please enter description';
                      }
                      return null;
                    },
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        child: Image.network(
                          UrlController.text,
                          fit: BoxFit.cover,
                        ),
                        height: 90,
                        width: 90,
                        margin: const EdgeInsets.only(top: 10, right: 10),
                        decoration: BoxDecoration(
                            border:
                            Border.all(color: Colors.grey, width: 2)),
                      ),
                      Expanded(
                          child: TextFormField(
                            focusNode: imageFocus,
                            decoration: InputDecoration(
                              labelText: 'Url',
                            ),
                            controller: UrlController,
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            onSaved: (value) {
                              Editedproduct = Product(
                                  id: Editedproduct.id,
                                  imageUrl: value,
                                  description: Editedproduct.description,
                                  price: Editedproduct.price,
                                  title: Editedproduct.title);
                            },
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter an image URL.';
                              }
                              if (!value.startsWith('http') &&
                                  !value.startsWith('https')) {
                                return 'Please enter a valid URL.';
                              }
                              if (!value.endsWith('.png') &&
                                  !value.endsWith('.jpg') &&
                                  !value.endsWith('.jpeg')) {
                                return 'Please enter a valid image URL.';
                              }
                              return null;
                            },
                          ))
                    ],
                  )
                ],
              ),
            ),
          )),
    );
  }
}
