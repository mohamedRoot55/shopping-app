import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Routes/EditOrAddNewProduct.dart';
import '../Providers/Products.dart';
import '../Widgets/appDrawer.dart';
import '../Providers/Auth.dart';
import '../Routes/Auth_Screen.dart';
import '../Routes/ViewAllProducts.dart';

class YourProducts extends StatefulWidget {
  static const String RouteName = '/YourProducts';

  @override
  _YourProductsState createState() => _YourProductsState();
}

class _YourProductsState extends State<YourProducts> {
  Future<void> _refreshInicator(BuildContext context) async {
    await Provider.of<Products>(context, listen: false)
        .FetchDataFromServer(true);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<Auth>(context);
    if (!authProvider.isAuthenticate) {
      // Navigator.of(context).pop();
      authProvider.logout();
    //  Navigator.of(context).pushReplacementNamed(ViewAllProducts.RouteName) ;
      return AuthScreen();
    } else {
      return Scaffold(
        drawer: appDrawer(),
        appBar: AppBar(
          actions: <Widget>[
            Container(
                margin: const EdgeInsets.only(right: 15),
                child: IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    Navigator.of(context)
                        .pushNamed(EditOrAddNewProduct.RouteName);
                  },
                ))
          ],
          title: Text('your products'),
          centerTitle: true,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
        ),
        body: FutureBuilder(
            future: _refreshInicator(context),
            builder: (ctx, snapShot) {
              return snapShot.connectionState == ConnectionState.waiting
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : RefreshIndicator(
                      onRefresh: () {
                        return _refreshInicator(context);
                      },
                      child: Container(
                        //height: 400,
                        child:
                            Consumer<Products>(builder: (_, products, child) {
                          final Data = products.allData;
                          return ListView.builder(
                              itemCount: Data.length,
                              itemBuilder: (_, index) {
                                return ListTile(
                                  leading: CircleAvatar(
                                    key: ValueKey(DateTime.now().toString()),
                                    backgroundImage: (Data[index].imageUrl ==
                                            null)
                                        ? null
                                        : NetworkImage(Data[index].imageUrl),
                                  ),
                                  title: Text(Data[index].title),
                                  trailing: Container(
                                    width: 100,
                                    child: Row(
                                      children: <Widget>[
                                        IconButton(
                                          icon: Icon(
                                            Icons.edit,
                                            color: Colors.grey,
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pushNamed(
                                                EditOrAddNewProduct.RouteName,
                                                arguments: Data[index].id);
                                          },
                                        ),
                                        IconButton(
                                          icon: Icon(Icons.delete,
                                              color: Colors.red),
                                          onPressed: () {
                                            Provider.of<Products>(context)
                                                .deleteSingleProduct(
                                                    Data[index].id);
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              });
                        }),
                      ),
                    );
            }),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).pushNamed(EditOrAddNewProduct.RouteName);
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.pinkAccent,
        ),
      );
    }
  }
}
