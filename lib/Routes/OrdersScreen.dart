
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Widgets/appDrawer.dart';
import 'package:provider/provider.dart';
import '../Providers/Orders.dart';

import '../Widgets/OrderItem.dart';

class OrdersScreen extends StatelessWidget {
  static const routeName = '/orders';

  @override
  Widget build(BuildContext context) {
    print('building orders');
    // final orderData = Provider.of<Orders>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Orders'),
      ),
      drawer: appDrawer(),
      body: FutureBuilder(
        future: Provider.of<Orders>(context, listen: false).FetchDataFromServer(),
        builder: (_, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (dataSnapshot.error != null) {
              // ...
              // Do error handling stuff
              return Center(
                child: Text('An error occurred!'),
              );
            } else {
              return Consumer<Orders>(
                builder: (ctx, orders, child) => ListView.builder(
                  itemCount: orders.getOrders.length,
                  itemBuilder: (_, i) => OrderItem(orders.getOrders[i]),
                ),
              );
            }
          }
        },
      ),
    );
  }
}




































//
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:shop_app_mohamed_ahmed_hassan/Routes/Auth_Screen.dart';
//import '../Widgets/appDrawer.dart';
//import 'package:provider/provider.dart';
//import '../Providers/Orders.dart';
//import 'package:intl/intl.dart';
//import 'dart:math';
//import '../Providers/Auth.dart';
//import '../Widgets/OrderItem.dart';
//
//class OrdersScreen extends StatefulWidget {
//  static const String RouteName = '/OrdersScreen';
//
//  @override
//  _OrdersScreenState createState() => _OrdersScreenState();
//}
//
//class _OrdersScreenState extends State<OrdersScreen> {
//  bool isExpanded = false;
//
////  @override
////  void didChangeDependencies() async {
////    super.didChangeDependencies();
////    await Provider.of<Orders>(context).FetchDataFromServer();
////  }
//
//  @override
//  void initState() {
//    Future.delayed(Duration.zero).then((_) {
//      Provider.of<Orders>(context, listen: false).FetchDataFromServer();
//    });
//    super.initState();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    final orderData  = Provider.of<Orders>(context);
//
//    List<Order> ordersList = orderData .getOrders;
//
//    final authProvider = Provider.of<Auth>(context);
//    if (!authProvider.isAuthenticate) {
//      //  Navigator.of(context).pop();
//      authProvider.logout();
//      return AuthScreen();
//    } else {
//      return Scaffold(
//        drawer: appDrawer(),
//        appBar: AppBar(
//          title: Text('Orders'),
//          centerTitle: true,
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.vertical(bottom: Radius.circular(20))),
//        ),
//        body: Container(
//          //padding: const EdgeInsets.all(15),
//          margin: const EdgeInsets.all(15),
//          child: ListView.builder(
//            itemCount: ordersList.length,
//            itemBuilder: (_, index) => OrderItem( orderData.getOrders[index]),
//          ),
//        ),
//      );
//    }
//  }
//}
