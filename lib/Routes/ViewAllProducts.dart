import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigment/pigment.dart';
import 'package:provider/provider.dart';
import 'package:shop_app_mohamed_ahmed_hassan/Routes/CartsScreen.dart';
import '../Providers/Products.dart';
import '../Widgets/ProductItem.dart';
import '../Providers/Carts.dart';
import '../Widgets/badge.dart';
import '../Widgets/appDrawer.dart';

enum ProductTybe { onlyFavorite, showAll }

class ViewAllProducts extends StatefulWidget {
  static const String RouteName = '/ViewALlProducts';

  @override
  _ViewAllProductsState createState() => _ViewAllProductsState();
}

class _ViewAllProductsState extends State<ViewAllProducts> {
  var isFav = false;
  var isLoading = false;
var isInit = true ;
  void _SelectedType(ProductTybe value) {
    if (value == ProductTybe.onlyFavorite) {
      setState(() {
        isFav = true;
      });
    } else {
      setState(() {
        isFav = false;
      });
    }
  }

  @override
 void didChangeDependencies(){
    // TODO: implement didChangeDependencies
    if(isInit){
      setState(() {
        isLoading = true;
      });
    }
    Provider.of<Products>(context).FetchDataFromServer(false).then((value){
      isLoading = false ;
//      setState(() {
//
//      });
    }) ;
    isInit = false ;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: appDrawer(),
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Pigment.fromString("#3F0B81"),
          centerTitle: true,
          title: Text(
            'Shopping',
            style: TextStyle(color: Pigment.fromString("#FFFFFF")),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
          actions: <Widget>[
            Consumer<Carts>(
              builder: (_, carts, child) {
                return Badge(
                  value: carts.getCarts().values.toList().length.toString(),
                  child: IconButton(
                    icon: Icon(
                      Icons.card_travel,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(CartsScreen.RouteName);
                    },
                  ),
                );
              },
            ),
            PopupMenuButton(
              icon: Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
              onSelected: _SelectedType,
              itemBuilder: (ctx) => [
                PopupMenuItem(
                  child: Text("only favorite"),
                  value: ProductTybe.onlyFavorite,
                ),
                PopupMenuItem(
                  child: Text("show all <3 "),
                  value: ProductTybe.showAll,
                )
              ],
            ),
          ],
        ),
        body:(isLoading) ? Center(child: CircularProgressIndicator(),): Container(
          padding: const EdgeInsets.all(15.0),
          child: Consumer<Products>(
            builder: (ctx, products, child) {
              List<Product> allData =
                  isFav ? products.OnlyFavorite : products.allData;
              return GridView.builder(
                itemCount: allData.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemBuilder: (_, index) {
                  return ChangeNotifierProvider.value(
                    value: allData[index],
                    child: ProductItem(),
                  );
                },
              );
            },
          ),
        ));
  }
}
