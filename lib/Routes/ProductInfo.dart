import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigment/pigment.dart';
import 'package:provider/provider.dart';
import '../Providers/Products.dart';

class ProductInfo extends StatelessWidget {
  static const String RouteName = './ProductInfo';

  Widget build(BuildContext context) {
    final productInfo =
        ModalRoute.of(context).settings.arguments as Map<String, Object>;

    return Scaffold(
//        appBar: AppBar(
//          centerTitle: true,
//          title: Text(
//            'Product Info',
//            style: TextStyle(color: Pigment.fromString("#0351C1")),
//          ),
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
//        ),
        body: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          expandedHeight: 200,
          pinned: true,
          flexibleSpace: FlexibleSpaceBar(
            title: Text(
              'Product Info',
              style: TextStyle(color: Pigment.fromString("#0351C1")),
            ),
            background: Hero(
              tag: productInfo["id"],
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.network(
                  productInfo['imageUrl'],
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    productInfo['title'],
                    style: TextStyle(color: Colors.teal),
                  ),
                  Text('\$' + productInfo['price'].toString(),
                      style: TextStyle(color: Colors.teal))
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                child: Center(
                  child: Text(productInfo['description'],
                      style: TextStyle(color: Colors.teal)),
                ),
              ),
              SizedBox(
                height: 1000,
              )
            ],
          ),
        ),
      ],
    ));
  }
}
