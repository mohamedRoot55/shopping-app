import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigment/pigment.dart';
import 'package:shop_app_mohamed_ahmed_hassan/helpers/CustomRoute.dart';
import './Providers/Auth.dart';
import './Routes/EditOrAddNewProduct.dart';
import './Routes/splashScreen.dart';
import './Routes/yourProducts.dart';
import './Routes/CartsScreen.dart';
import './Routes/OrdersScreen.dart';
import './Routes/ViewAllProducts.dart';
import 'package:provider/provider.dart';
import './Providers/Products.dart';
import './Routes/ProductInfo.dart';
import './Providers/Carts.dart';
import './Providers/Orders.dart';
import './Routes/Auth_Screen.dart';

void main() {
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Auth()),
        ChangeNotifierProxyProvider<Auth, Products>(
          builder: (ctx, auth, previousProducts) {
            return Products(auth.token, auth.UserId);
          },
        ),
        ChangeNotifierProvider.value(value: Carts()),
        ChangeNotifierProxyProvider<Auth, Orders>(
          builder: (ctx, auth, ordersData) {
            return Orders(auth.UserId, auth.token,
                ordersData != null ? ordersData.getOrders : []);
          },
        ),
      ],
      child: Consumer<Auth>(
        builder: (_, auth, child) {
          return MaterialApp(
            title: 'make your shopping',
            theme: ThemeData(
                primaryColor: Pigment.fromString("#FE634E"),
                accentColor: Pigment.fromString("#FDA371"),
                fontFamily: 'Raleway',
                pageTransitionsTheme: PageTransitionsTheme(builders: {

                  TargetPlatform.android: TranstionBuilder(),
                  TargetPlatform.iOS: TranstionBuilder(),
                })),
            home: auth.isAuthenticate
                ? ViewAllProducts()
                : FutureBuilder(
              future: auth.autoLogin(),
              builder: (ctx, snapshot) =>
              snapshot.connectionState == ConnectionState.waiting
                  ? SplashScreen()
                  : AuthScreen(),
            ),
            routes: {
              ProductInfo.RouteName: (ctx) => ProductInfo(),
              CartsScreen.RouteName: (ctx) => CartsScreen(),
              ViewAllProducts.RouteName: (ctx) => ViewAllProducts(),
              OrdersScreen.routeName: (ctx) => OrdersScreen(),
              YourProducts.RouteName: (ctx) => YourProducts(),
              EditOrAddNewProduct.RouteName: (ctx) => EditOrAddNewProduct(),
              AuthScreen.routeName: (ctx) => AuthScreen()
            },
          );
        },
      ),
    );
  }
}
